<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .negro{
            background-color: black;
            border: 1px solid black;
            color: white;
        }
        .gris{
            background-color: #ccc;
        }
    </style>
</head>
<body>
    <?php
    // Si tipo es claro => coloco la clase gris
    // Si tipo es oscuro => coloco la clase negro
    $tipo="oscuro";

    if ($tipo=="claro") {
        $clase = "gris";
    } else{
        $clase = "negro";
    }
    ?>
    <div class= <?= $clase?>>Ejemplo de clase</div>
</body>
</html>