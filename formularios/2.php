<?php
    // Lo mismo que el ejercicio 1 pero siempre mostramos el formulario
    $nombre="";

    if (isset($_GET["enviar"])) {
        $nombre = $_GET["nombre"];
    }
    ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

    <form action="">
        <div>
            <label for="nombre">Nombre</label>
            <input type="text" name="nombre" id= "nombre">
        </div>
        <div>
            <button name="enviar">Enviar</button>
        </div>
    </form>
    <div>
        Nombre: <?= $nombre?>
    </div>

</body>
</html>