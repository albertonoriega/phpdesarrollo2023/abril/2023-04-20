<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php

    $nombre="";

    //Comprobar si se ha cargado el formulario

    // Si no se ha pulsado el boton de enviar
    if (!isset($_GET["enviar"])) {
                
    ?>
    
    <form action="">
        <div>
            <label for="nombre">Nombre</label>
            <input type="text" name="nombre" id= "nombre">
        </div>
        <div>
            <button name="enviar">Enviar</button>
        </div>
    </form>

    <?php

    } else {
    // Cuando se ha pulsado el botón => mostramos resultados
        $nombre = $_GET["nombre"];
    }

    ?>

    <div>
        Nombre: <?= $nombre?>
    </div>

</body>
</html>