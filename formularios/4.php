<?php
$mensaje = "";

if (isset($_GET["enviar"])) {
    $mensaje = $_GET["mensaje"];
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form action="">
        <div>
            <label for="mensaje">Mensaje:</label>
            <input type="text" id="mensaje" name="mensaje">
        </div>
        <div>
            <button name="enviar">Enviar</button>
        </div>
    </form>

    <script>
        <?php
        if (!empty($mensaje)) {
            // echo "alert('$mensaje')"; // Si escribes todo en php
        ?>
            alert("<?= $mensaje ?>");
        <?php
        }
        ?>
    </script>
</body>

</html>