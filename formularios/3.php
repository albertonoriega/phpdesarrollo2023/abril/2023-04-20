<?php
$ancho = "";
$radio = "";
$color = "claro"; // Valor por defecto => claro

if (isset($_GET["enviar"])) {
    $ancho = $_GET["ancho"];
    $radio = $_GET["radio"];
    $color = $_GET["color"];
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .cajas {
            border: <?= $ancho ?>px solid black;
            padding: 10px;
        }

        .redondeado {
            border-radius: <?= $radio ?>px;
        }

        .oscuro {
            background-color: black;
            color: white;
            border-color: red;
        }

        .claro {
            background-color: #ccc;
        }
    </style>
</head>

<body>
    <form action="">
        <div>
            <label for="ancho">Ancho borde:</label>
            <input type="number" id="ancho" name="ancho">
        </div>
        <div>
            <label for="">Radio de las cajas:</label>
            <input type="number" id="radio" name="radio">
        </div>
        <div>
            <label for="color">Color de las cajas (claro/oscuro):</label>
            <input type="text" id="color" name="color">
        </div>
        <div>
            <button name="enviar">Cambiar</button>
        </div>
    </form>


    <div class="<?= $color ?> cajas redondeado">Ramon</div>
    <div class="<?= $color ?> cajas redondeado">Alpe</div>
    <div class="<?= $color ?> cajas redondeado">Desarrollo</div>
</body>

</html>